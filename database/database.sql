CREATE DATABASE dofus;

USE dofus;

CREATE TABLE `etapa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `recompensa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mostros` (
  `id` int(11) NOT NULL,
  `idetapa` int(11) NOT NULL,
  `moob` varchar(100) NOT NULL,
  `img` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `etapa`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `mostros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idetapa` (`idetapa`);

ALTER TABLE `mostros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `mostros`
  ADD CONSTRAINT `mostros_ibfk_1` FOREIGN KEY (`idetapa`) REFERENCES `etapa` (`id`);
COMMIT;

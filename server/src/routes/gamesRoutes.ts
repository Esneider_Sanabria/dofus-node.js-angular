import express, { Router } from 'express';

import gamesController from '../controllers/gamesController';

class GameRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {

        // etapas
        this.router.get('/etapas', gamesController.etapas);
        
        // monstruos por etapas
        this.router.get('/:id', gamesController.list);

        // monstruo
        this.router.get('/monstruo/:id', gamesController.getMonstruo);

        // crud
        this.router.post('/', gamesController.create);
        this.router.put('/:id', gamesController.update);
        this.router.delete('/:id', gamesController.delete);
    }

}

export default new GameRoutes().router;


import { Request, Response } from 'express';


import pool from '../database';

class GamesController {

    public async etapas(req: Request, res: Response): Promise<void>{
        const etapas = await pool.query('SELECT * FROM etapa');
        res.json(etapas);
        console.log(etapas);
    }

    public async getMonstruo(req: Request, res: Response): Promise<void>{
        const { id } = req.params;
        const etapas = await pool.query('SELECT * FROM mostros WHERE id = ?', [id]);
        res.json(etapas);
        console.log(etapas);
    }

    public async list(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const games = await pool.query('SELECT m.*, t.nombre as tipo FROM mostros m inner JOIN etapa e on e.id = m.idetapa INNER JOIN tipo t on t.id = m.idtipo WHERE e.id = ?', [id]);
        res.json(games);
        console.log(games);
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO mostros set ?', [req.body]);
        res.json({ message: 'Monstruo guardado' });
        console.log("registrandos");
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldGame = req.body;
        await pool.query('UPDATE mostros set ? WHERE id = ?', [req.body, id]);
        res.json({ message: "Monstruo actualizado" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM mostro WHERE id = ?', [id]);
        res.json({ message: "Monstruo" });
    }
}

const gamesController = new GamesController;
export default gamesController;
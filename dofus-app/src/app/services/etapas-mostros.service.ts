import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Monstruo } from '../models/Monstruo';

@Injectable({
  providedIn: 'root'
})
export class EtapasMostrosService {

  url = 'http://localhost:3000/api';
  constructor(private http: HttpClient) { }

  getMostros(id) {
    return this.http.get(`${this.url}/games/${id}`);
  }

  getMonstruo(id){
    return this.http.get(`${this.url}/games/monstruo/${id}`);
  }

  getEtapas() {
    return this.http.get(`${this.url}/games/etapas`);
  }

  saveMonstruo(monstruo: Monstruo) {
    return this.http.post(`${this.url}/games/`, monstruo);
  }

  deleteMonstruo(id){
    return this.http.delete(`${this.url}/games/${id}`);
  }

  updateMonstruo(id, updateMonstruo){
    return this.http.put(`${this.url}/games/${id}`, updateMonstruo);
  }
}
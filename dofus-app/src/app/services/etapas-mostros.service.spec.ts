import { TestBed } from '@angular/core/testing';

import { EtapasMostrosService } from './etapas-mostros.service';

describe('EtapasMostrosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EtapasMostrosService = TestBed.get(EtapasMostrosService);
    expect(service).toBeTruthy();
  });
});

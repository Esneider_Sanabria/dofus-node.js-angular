import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoMonstruoComponent } from './nuevo-monstruo.component';

describe('NuevoMonstruoComponent', () => {
  let component: NuevoMonstruoComponent;
  let fixture: ComponentFixture<NuevoMonstruoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoMonstruoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoMonstruoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

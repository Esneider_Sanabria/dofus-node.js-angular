import { Component, OnInit, HostBinding } from '@angular/core';
import { Monstruo } from '../../models/Monstruo';
import { EtapasMostrosService } from '../../services/etapas-mostros.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-nuevo-monstruo',
  templateUrl: './nuevo-monstruo.component.html',
  styleUrls: ['./nuevo-monstruo.component.css']
})
export class NuevoMonstruoComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  monstruo: Monstruo = {
    id: 0,
    idetapa: 0,
    idtipo: 0,
    moob: '',
    img: ''
  };

  constructor(private etapaMostruosService: EtapasMostrosService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    console.log(params);
    if (params.id){
      const a = this.etapaMostruosService.getMonstruo(params.id).subscribe(
        res => this.monstruo = res[0],
        error => console.log(error)
      );

      
    }
  
  }

  saveNewMonstruo(){
    this.etapaMostruosService.saveMonstruo(this.monstruo)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/etapa/', this.monstruo.idetapa]);
      },
      error => console.log(error)
    )
  }

}

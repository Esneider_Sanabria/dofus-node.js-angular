import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEtapasRecompensaComponent } from './lista-etapas-recompensa.component';

describe('ListaEtapasRecompensaComponent', () => {
  let component: ListaEtapasRecompensaComponent;
  let fixture: ComponentFixture<ListaEtapasRecompensaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEtapasRecompensaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEtapasRecompensaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { EtapasMostrosService } from '../../services/etapas-mostros.service';

@Component({
  selector: 'app-lista-etapas-recompensa',
  templateUrl: './lista-etapas-recompensa.component.html',
  styleUrls: ['./lista-etapas-recompensa.component.css']
})
export class ListaEtapasRecompensaComponent implements OnInit {

  Etapas: any = [];

  constructor(private etapasMostrosService: EtapasMostrosService) {

   }

  ngOnInit() {

    this.etapasMostrosService.getEtapas().subscribe(
      res => {
        this.Etapas = res;
      },
      error => console.log(error),
    )
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { EtapasMostrosService } from '../../services/etapas-mostros.service';

@Component({
  selector: 'app-lits-etapas',
  templateUrl: './lits-etapas.component.html',
  styleUrls: ['./lits-etapas.component.css']
})
export class LitsEtapasComponent implements OnInit {
  id: number;
  Monstruos: any = [];

  constructor(private etapasMostrosService: EtapasMostrosService, private activatedRoute:ActivatedRoute) {
    activatedRoute.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit() {
    this.etapasMostrosService.getMostros(this.id).subscribe(
      res => {
        this.Monstruos = res;
      },
      error => console.error(error),
    );
  }

}

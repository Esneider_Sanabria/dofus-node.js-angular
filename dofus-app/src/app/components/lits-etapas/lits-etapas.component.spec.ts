import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LitsEtapasComponent } from './lits-etapas.component';

describe('LitsEtapasComponent', () => {
  let component: LitsEtapasComponent;
  let fixture: ComponentFixture<LitsEtapasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LitsEtapasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LitsEtapasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

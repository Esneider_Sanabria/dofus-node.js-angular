import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule  } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LitsEtapasComponent } from './components/lits-etapas/lits-etapas.component';
import { NavigationComponent } from './components/navigation/navigation.component';

import { EtapasMostrosService } from './services/etapas-mostros.service';
import { ListaEtapasRecompensaComponent } from './components/lista-etapas-recompensa/lista-etapas-recompensa.component';
import { NuevoMonstruoComponent } from './components/nuevo-monstruo/nuevo-monstruo.component';

@NgModule({
  declarations: [
    AppComponent,
    LitsEtapasComponent,
    NavigationComponent,
    ListaEtapasRecompensaComponent,
    NuevoMonstruoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    EtapasMostrosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

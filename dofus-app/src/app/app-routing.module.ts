import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LitsEtapasComponent } from './components/lits-etapas/lits-etapas.component';
import { ListaEtapasRecompensaComponent } from './components/lista-etapas-recompensa/lista-etapas-recompensa.component';
import { NuevoMonstruoComponent } from './components/nuevo-monstruo/nuevo-monstruo.component';

const appRoutes: Routes = [
  { path: 'etapa/:id', component: LitsEtapasComponent },
  { path: 'lista', component: ListaEtapasRecompensaComponent, pathMatch: 'full' },
  { path: 'nuevo-monstruo', component: NuevoMonstruoComponent, pathMatch: 'full' },
  { path: 'edit-monstruo/:id', component: NuevoMonstruoComponent, pathMatch: 'full' },
  { path: '**', component: LitsEtapasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }